<?php

$servername = "localhost";
$username = "hadi";
$password = "";

try {
    $conn = new PDO("mysql:host=$servername;dbname=dorb", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    die();
    }

?>
