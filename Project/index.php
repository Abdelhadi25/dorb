<?php
session_start();

require_once '../Private/connection.php';

if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 'home';
}

if (!(isset($_SESSION['rol']))) {
    $_SESSION['rol'] = false;
}
?>
<!DOCTYPE html>
<html>
<!--UgfR2a9yH5e5ZYwqg8kW7MJxzazje3jCPhEPafe0-->
<head>
    <title>DORB</title>
    <link rel="stylesheet" href="CSS/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<!--    <meta http-equiv="refresh" content="300; url='php/loguit.php'" >-->
</head>

<body>
<header>
    <?php include 'includes/nav.inc.php' ?>
</header>
<div class="container">
    <?php include 'includes/' . $page . '.inc.php'; ?>
</div>
</body>

</html>
