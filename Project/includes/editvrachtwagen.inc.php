<?php

$sql = 'SELECT vrachtwagen_id, FK_soort_id, kenteken, apk, status FROM vrachtwagen WHERE vrachtwagen_id = :id';
$sth = $conn->prepare($sql);
$sth ->execute(array(
    ':id' => $_POST['id'],
));

$result = $sth->fetch();

?>



<div class="plaatsorder">
    <form action="php/editvrachtwagen.php?id=<?= $result['vrachtwagen_id']?>" method="post">

        <div class="col-sm-12">
            <label class="ordrlabel">Soort&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
            <select name="soort">
                <option value="1">CF 300 FA</option>
                <option value="2">CF 410 FTN</option>
                <option value="3">XF 430 FTM</option>
            </select>
        </div>
        <div class="col-sm-12">
            <label class="ordrlabel">Kenteken</label>
            <input class="aantal" type="text" name='kenteken'  value="<?= $result['kenteken']  ?>" required/>        </div>
        <div class="col-sm-12">
            <label class="ordrlabel">APK&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
            <input class="aantal" type="text" name='apk'  value="<?= $result['apk']  ?>" required/>        </div>
        <div class="col-sm-12">
            <label class="ordrlabel">Status&nbsp;&nbsp;</label>
            <select name="status" >
                <option value="Beschikbaar">Beschikbaar</option>
                <option value="Onderhoud">Onderhoud</option>
            </select>
        </div>



        <button type="submit" class="btn btn-success" style="float: right">Submit</button>
    </form>
</div>