<?php
$sql = 'SELECT klant_id FROM klanten WHERE FK_login_id = :FK_login_id';
$sth = $conn->prepare($sql);
$sth->execute(array(
    ':FK_login_id' => $_SESSION['id']
));
$result1 = $sth->fetch();


$sql = 'SELECT * FROM orders WHERE FK_klantnummer = :FK_klantnummer';
$sth = $conn->prepare($sql);
$sth->execute(array(
        ':FK_klantnummer' => $result1['klant_id']
));
$result = $sth->fetchAll();
?>


    <div class="insidenav">
        <ul class="ulhelp">
            <li class="liicter"><a href="index.php?page=mijnorders"><p class="pnav">Mijn orders</p></a></li>
        </ul>
        <ul class="ulhelp">
            <li class="liicter"><a href="index.php?page=klant"><p class="pnav">Plaats een order</p></a></li>
        </ul>
    </div>
<?php
foreach ($result as $item) {
?>


<div class="divorder" >
    <div class="annuleren"><a href="php/annuleren.php?id=<?= $item['order_id']?>" onclick="return confirm('Wil je deze order annuleren?')" >ANNULEREN</a></div>
    <div class="edit"><a href="index.php?page=editorder&id=<?= $item['order_id']?>">EDIT</a></div>
    <div class="ticketdivicter">

        <table class="table1">
            <thead>
            <tr>
                <th class="th12">Aantal Pallets</th>
                <th class="th12">Soort Pallets</th>
                <th class="th12">Beschrijving</th>
                <th class="th12">Postcode</th>
                <th class="th12">Huisnummer</th>
                <th class="th12">Straat</th>
                <th class="th12">Stad</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="td12"><?= $item['aantal'] ?></td>
                <td class="td12"><?= $item['soort'] ?></td>
                <td class="td12"><?= $item['beschrijving'] ?></td>
                <td class="td12"><?= $item['postcode'] ?></td>
                <td class="td12"><?= $item['huisnummer'] ?></td>
                <td class="td12"><?= $item['straat'] ?></td>
                <td class="td12"><?= $item['stad'] ?></td>
            </tr>
            </tbody>
        </table>
    </div>

</div>
<?php } ?>