<?php

$id = $_GET['id'];

$sql = 'SELECT * FROM orders WHERE order_id = :order_id';
$sth = $conn->prepare($sql);
$sth->execute(array(
    ':order_id' => $id
));

$result = $sth->fetch();
?>

<div class="plaatsorder">
<form action="php/editorder.php?id=<?= $result['order_id']?>" method="post">

    <div class="col-sm-12">
        <label class="ordrlabel">Aantal pallets:</label>
        <input class="aantal" type="text" name='aantal'  value="<?= $result['aantal']  ?>" required/>
    </div>
    <div class="col-sm-12">
        <label class="ordrlabel">Soortpallets:</label>
        <select name="soortpallet" >
            <option value="1x1x1">1x1x1</option>
            <option value="1x1x2">1x1x2</option>
            <option value="1x1x3">1x1x3</option>
            <option value="1x2x1">1x2x1</option>
            <option value="1x2x2">1x2x2</option>
            <option value="1x2x3">1x2x3</option>
            <option value="2x2x1">2x2x1</option>
            <option value="2x2x2">2x2x2</option>
            <option value="2x2x3">2x2x3</option>
        </select>
    </div>
    <div class="col-sm-12">
        <label class="ordrlabel">Beschrijving:</label>
        <textarea class="form-control" rows="3" name="beschrijving" value="" id="comment"><?= $result['beschrijving'] ?></textarea>
    </div>

    <div class="col-sm-12" style="margin-top: 50px; height: 185px;">
        <div class="col-sm-12">
            <label class="postcode">Postcode:</label>
            <input class="postcodeinput" type="text" name='postcode' value="<?= $result['postcode']  ?>" placeholder="Postcode" required/>
            <label class="postcode">Huisnummer:</label>
            <input class="postcodeinput" type="text" name='huisnummer' value="<?= $result['huisnummer']  ?>" placeholder="hr" required/>
        </div>
        <div class="col-sm-12">
            <label class="ordrlabel" >Straat: &nbsp</label>
            <input class="aantal" type="text" name='straat' value="<?= $result['straat']  ?>" placeholder="Straat"/>
            <label class="postcode">Stad: &nbsp &nbsp &nbsp &nbsp</label>
            <input class="postcodeinput" type="text" name='stad' value="<?= $result['stad']  ?>" placeholder="Stad" required/>
        </div>
    </div>
    <button type="submit" class="btn btn-success" style="float: right">Submit</button>
</form>
</div>
