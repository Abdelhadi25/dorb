<?php

$status = '2';
$koppel_id = ('koppel_id');

if(isset($_GET['id'])) {

$sql = 'INSERT INTO koppeltbl (FK_order_id,FK_vrachwagen_id, FK_chauffeur_id, datum) VALUE (:FK_order_id,:FK_vrachwagen_id, :FK_chauffeur_id, now())';
$sth = $conn->prepare($sql);
$sth ->execute(array(
    ':FK_order_id' => $_GET['id'],
    ':FK_vrachwagen_id' => $_GET['vw'],
    ':FK_chauffeur_id' => $_POST['chauffeurid']
));

$rsid = $conn -> lastInsertId();

$sql = 'UPDATE koppeltbl SET rit_id = :rit_id WHERE koppel_id = :koppel_id';
$sth = $conn->prepare($sql);
$sth->execute(array(
    ':rit_id' => $rsid,
    ':koppel_id' => $rsid,
));

$sql = 'UPDATE orders SET status = :status WHERE order_id = :order_id';
$sth = $conn->prepare($sql);
$sth ->execute(array(
    ':status' => $status,
    ':order_id' => $_GET['id']
));
}

$sql = 'SELECT soortvrachtwagen.inhoud FROM vrachtwagen INNER JOIN soortvrachtwagen ON FK_soort_id = soort_id WHERE vrachtwagen_id = :vrachtwagen_id';
$sth = $conn->prepare($sql);
$sth->execute(array(
    ':vrachtwagen_id' => $_GET['vw']
));
$result = $sth->fetch();

$restruimte = $result['inhoud'] - $_GET['mt'];


$sql = 'SELECT order_id, aantal, soortpallets.soort AS soort, soortpallets.maat, beschrijving, postcode, huisnummer, straat, stad FROM orders INNER JOIN soortpallets on orders.soort = soortpallets.soort_id WHERE orders.maat < :restruimte AND status = 1';
$sth = $conn->prepare($sql);
$sth ->execute(array(
    ':restruimte' => $restruimte
));
$orderskan = $sth->fetchAll();


?>

<div class="insidenav">
    <ul class="ulhelp">
        <li class="liicter"><a href="index.php?page=vrachtwagens"><p class="pnav">Vrachtwagens</p></a></li>
    </ul>
    <ul class="ulhelp" >
        <li class="liicter"><a href="index.php?page=orders"><p class="pnav" style="margin-left: 40px;">Orders</p></a></li>
    </ul>
</div>

<h1 style="text-align: center">ER PAST NOG MEER IN DEZE VRACHTWAGEN</h1>
<h1 style="text-align: center">Kies andere orders</h1>
<h1 style="text-align: center">Toch vertrekken? </h1>


<button type="submit" class="btn btn-primary" style="margin-left: 485px !important;"><a href="index.php?page=orders" style="color: #ffff">TOCH VERTREKKEN</a></button>

<?php
foreach ($orderskan as $item) {
    $maat = $item['maat'] * $item['aantal'];
    ?>


    <div class="divorder" >
        <form method="post" action="php/meerorders.php">
            <input type="hidden" name="orderid" value="<?= $item['order_id']?>">
            <input type="hidden" name="ritid" value="<?= $rsid ?>">
            <input type="hidden" name="vrachtwagenid" value="<?= $_GET['vw'] ?>">
            <input type="hidden" name="chauffeurid" value="<?=$_POST['chauffeurid']?>">
            <button type="submit" class="edit">Toevoegen</button>
        </form>
        <div class="ticketdivicter">

            <table class="table1">
                <thead>
                <tr>
                    <th class="th12">Aantal Pallets</th>
                    <th class="th12">Soort Pallets</th>
                    <th class="th12">Beschrijving</th>
                    <th class="th12">Postcode</th>
                    <th class="th12">Huisnummer</th>
                    <th class="th12">Straat</th>
                    <th class="th12">Stad</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="td12"><?= $item['aantal'] ?></td>
                    <td class="td12"><?= $item['soort'] ?></td>
                    <td class="td12"><?= $item['beschrijving'] ?></td>
                    <td class="td12"><?= $item['postcode'] ?></td>
                    <td class="td12"><?= $item['huisnummer'] ?></td>
                    <td class="td12"><?= $item['straat'] ?></td>
                    <td class="td12"><?= $item['stad'] ?></td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
<?php } ?>

