<?php

$sql = 'SELECT vrachtwagen_id,inhoud ,soort, apk, kenteken,status FROM vrachtwagen INNER JOIN soortvrachtwagen ON FK_soort_id = soort_id WHERE inhoud > :maat  ORDER BY soort';
$sth = $conn ->prepare($sql);
$sth -> execute(array(
    ':maat' => $_GET['mt']
));
$result = $sth->fetchAll();

?>
<div class="container-tabel">

    <table class="table121">
        <thead>
        <tr>
            <th class="th1" ></th>
            <th class="th1">Soort</th>
            <th class="th1">Kenteken</th>
            <th class="th1">APK</th>
            <th class="th1">Inhoud</th>
        </tr>
        </thead>
        <form action="index.php?page=chosechauffeur&id=<?= $_GET['id'] ?>&mt=<?= $_GET['mt'] ?>" method="post">
        <tbody>
        <?php foreach ($result as $item)  { ?>
            <tr>
                <td><input type="radio" name="vrachtwagenid" value="<?=  $item['vrachtwagen_id']  ?>"></td>
                <td><?= $item['soort'] ?></td>
                <td><?= $item['kenteken'] ?></td>
                <td><?= $item['apk'] ?></td>
                <td><?= $item['inhoud'] ?></td>
            </tr>
        <?php } ?>

        </tbody>
    </table>
</div>
    <button type="submit" class="btn btn-success" style="float: right">Submit</button>
</form>
