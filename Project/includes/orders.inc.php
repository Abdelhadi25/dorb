<?php

$status = '1';

$sql = 'SELECT order_id, aantal, soortpallets.soort AS soort, soortpallets.maat, beschrijving, postcode, huisnummer, straat, stad FROM orders INNER JOIN soortpallets on orders.soort = soortpallets.soort_id WHERE status = :status';
$sth = $conn->prepare($sql);
$sth->execute(array(
        ':status' => $status
));
$result = $sth->fetchAll();

?>

<div class="insidenav">
    <ul class="ulhelp">
        <li class="liicter"><a href="index.php?page=vrachtwagens"><p class="pnav">Vrachtwagens</p></a></li>
    </ul>
    <ul class="ulhelp" >
        <li class="liicter"><a href="index.php?page=orders"><p class="pnav" style="margin-left: 40px;">Orders</p></a></li>
    </ul>
</div>


<?php
foreach ($result as $item) {
    $maat = $item['maat'] * $item['aantal']
    ?>


    <div class="divorder" >
        <div class="edit"><a href="index.php?page=ordersverwerken&id=<?= $item['order_id']?>&mt=<?= $maat ?>">Verwerken</a></div>
        <div class="ticketdivicter">

            <table class="table1">
                <thead>
                <tr>
                    <th class="th12">Aantal Pallets</th>
                    <th class="th12">Soort Pallets</th>
                    <th class="th12">Beschrijving</th>
                    <th class="th12">Postcode</th>
                    <th class="th12">Huisnummer</th>
                    <th class="th12">Straat</th>
                    <th class="th12">Stad</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="td12"><?= $item['aantal'] ?></td>
                    <td class="td12"><?= $item['soort'] ?></td>
                    <td class="td12"><?= $item['beschrijving'] ?></td>
                    <td class="td12"><?= $item['postcode'] ?></td>
                    <td class="td12"><?= $item['huisnummer'] ?></td>
                    <td class="td12"><?= $item['straat'] ?></td>
                    <td class="td12"><?= $item['stad'] ?></td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
<?php } ?>

