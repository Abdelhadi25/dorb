<?php


$sql = 'SELECT planners.planner_id AS id, planners.voornaam, planners.tussenvoegsel ,planners.achternaam, login_id ,gebruikersnaam, rol FROM planners INNER JOIN login ON planners.FK_login_id = login_id INNER JOIN rollen ON FK_rol_id = rol_id 
UNION
SELECT helpdesk.helpdesk_id, helpdesk.voornaam, helpdesk.tussenvoegsel ,helpdesk.achternaam , login_id ,gebruikersnaam, rol FROM helpdesk INNER JOIN login ON helpdesk.FK_login_id = login_id INNER JOIN rollen ON FK_rol_id = rol_id';
$sth = $conn ->prepare($sql);
$sth -> execute();
$result = $sth->fetchAll();




?>

<div class="container-tabel">
    <table class="table12">
        <thead>
        <tr>
            <th class="th1">Voornaam</th>
            <th class="th1">Tussenvoegsel</th>
            <th class="th1">Achternaam</th>
            <th class="th1">Gebruikersnaam</th>
            <th class="th1">Rol</th>
            <th class="th1">Delete</th>
            <th class="th1">Edit</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($result as $item)  { ?>
        <tr>
            <td><?= $item['voornaam'] ?></td>
            <td><?= $item['tussenvoegsel'] ?></td>
            <td><?= $item['achternaam'] ?></td>
            <td><?= $item['gebruikersnaam'] ?></td>
            <td><?= $item['rol'] ?></td>
            <td>
                <form  action="php/delete.php" method="POST">
                    <input type="hidden" value="<?=$item['id']?>" name="id">
                    <input type="hidden" value="<?=$item['rol']?>" name="rol">
                    <input type="hidden" value="<?=$item['login_id']?>" name="login_id">
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>


            </td>
            <td>
                <form action="index.php?page=edit" method="POST">
                    <input type="hidden" value="<?=$item['id']?>" name="id">
                    <input type="hidden" value="<?=$item['rol']?>" name="rol">
                    <input type="hidden" value="<?=$item['login_id']?>" name="login_id">
                    <button type="submit" class="btn btn-info">Edit</button>
                </form>

            </td>
        </tr>
        <?php } ?>

        </tbody>
    </table>


        <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">MEDEWERKER TOEVOEGEN</button>
        <div id="demo" class="collapse">
            <div class="btn-group btn-group-justified">
                <a href="index.php?page=planneradd" class="btn btn-primary">Planner</a>
                <a href="index.php?page=helpdeskadd" class="btn btn-primary">Helpdesk</a>
            </div>
        </div>
</div>






