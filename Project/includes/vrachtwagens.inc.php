<?php

$sql = 'SELECT vrachtwagen_id ,soort, apk, kenteken,status FROM vrachtwagen INNER  JOIN soortvrachtwagen ON FK_soort_id = soort_id ORDER BY soort';
$sth = $conn ->prepare($sql);
$sth -> execute();
$result = $sth->fetchAll();

?>

<div class="insidenav">
    <ul class="ulhelp">
        <li class="liicter"><a href="index.php?page=vrachtwagens"><p class="pnav">Vrachtwagens</p></a></li>
    </ul>
    <ul class="ulhelp" >
        <li class="liicter"><a href="index.php?page=orders"><p class="pnav" style="margin-left: 40px;">Orders</p></a></li>
    </ul>
</div>

<div class="btn-group btn-group-justified" style="z-index: 1; margin-top: 40px !important;">
    <a onclick="openpopup()" class="btn btn-primary">ADD nieuwe vrachtwagen</a>
</div>
<div id="popup">
    <form action="php/vrachtwagentoevoegen.php" method="post">
        <div class="col-sm-12">
            <label class="ordrlabel">Soort vrachtwagen:</label>
            <select name="soortvrachtwagen">
                <option value="1">CF 300 FA</option>
                <option value="2">CF 410 FTN</option>
                <option value="3">XF 430 FTM</option>
            </select>
        </div>
        <input type="text" name='kenteken' placeholder="Kenteken" required/>
        <input type="date" name='apk' placeholder="APK" required/>
        <p class="p-container">
            <input type="submit" value="Toevoegen">
        </p>

    </form>
</div>

<div class="container-tabel">
    <table class="table121">
        <thead>
        <tr>
            <th class="th1">Soort</th>
            <th class="th1">Kenteken</th>
            <th class="th1">APK</th>
            <th class="th1">Status</th>
            <th class="th1" >Delete</th>
            <th class="th1" >Edit</th>
        </tr><x></x>
        </thead>
        <tbody>
        <?php foreach ($result as $item)  { ?>
            <tr>
                <td><?= $item['soort'] ?></td>
                <td><?= $item['kenteken'] ?></td>
                <td><?= $item['apk'] ?></td>
                <td><?= $item['status'] ?></td>
                <td>
                    <form  action="php/deletevrachtwagen.php" method="POST">
                        <input type="hidden" value="<?=$item['vrachtwagen_id']?>" name="id">
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>


                </td>
                <td>
                    <form action="index.php?page=editvrachtwagen" method="POST">
                        <input type="hidden" value="<?=$item['vrachtwagen_id']?>" name="id">
                        <button type="submit" class="btn btn-info">Edit</button>
                    </form>

                </td>
            </tr>
        <?php } ?>

        </tbody>
    </table>
</div>



<script>
    function openpopup() {
        var x = document.getElementById("popup");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
</script>




