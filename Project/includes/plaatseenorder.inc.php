<?php

// De headers worden altijd meegestuurd als array
$headers = array();
$headers[] = 'X-Api-Key: UgfR2a9yH5e5ZYwqg8kW7MJxzazje3jCPhEPafe0';

// De URL naar de API call
$url = 'https://api.postcodeapi.nu/v2/addresses/?postcode=1234AB';

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

// Indien de server geen TLS ondersteunt kun je met
// onderstaande optie een onveilige verbinding forceren.
// Meestal is dit probleem te herkennen aan een lege response.
// curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

// De ruwe JSON response
$response = curl_exec($curl);

// Gebruik json_decode() om de response naar een PHP array te converteren
$data = json_decode($response);

curl_close($curl);
?>

<div class="insidenav">
    <ul class="ulhelp">
        <li class="liicter"><a href="index.php?page=registeereenklant"><p class="pnav">Registeer een klant</p></a></li>
    </ul>
    <ul class="ulhelp">
        <li class="liicter"><a href="index.php?page=plaatseenorder"><p class="pnav">Plaats een order</p></a></li>
    </ul>
</div>


<div class="plaatsorder">
<form action="php/plaatsoorder.php" method="post">

    <div class="col-sm-12">
    <label class="ordrlabel">Aantal pallets:</label>
    <input class="aantal" type="text" name='aantal'  required/>
    </div>
    <div class="col-sm-12">
        <label class="ordrlabel">Soortpallets:</label>
    <select name="soortpallet">
        <option value="1">1x1x1</option>
        <option value="2">1x1x2</option>
        <option value="3">1x1x3</option>
        <option value="4">1x2x1</option>
        <option value="5">1x2x2</option>
        <option value="6">1x2x3</option>
        <option value="7">2x2x1</option>
        <option value="8">2x2x2</option>
        <option value="9">2x2x3</option>
    </select>
        <label class="ordrlabel1">Klantnummer:</label>
        <input class="postcodeinput" type="text" name='klantnummer'  placeholder="Klantnummer" required/>
    </div>
    <div class="col-sm-12">
        <label class="ordrlabel">Beschrijving:</label>
        <textarea class="form-control" rows="3" name="beschrijving" id="comment"></textarea>
    </div>

    <div class="col-sm-12" style="margin-top: 50px; height: 185px;">
    <div class="col-sm-12">
        <label class="postcode">Postcode:</label>
        <input class="postcodeinput" type="text" name='postcode'  placeholder="Postcode" required/>
        <label class="postcode">Huisnummer:</label>
        <input class="postcodeinput" type="text" name='huisnummer'  placeholder="hr" required/>
    </div>
        <div class="col-sm-12">
            <label class="ordrlabel" >Straat: &nbsp</label>
            <input class="aantal" type="text" name='straat'  placeholder="Straat"/>
            <label class="postcode">Stad: &nbsp &nbsp &nbsp &nbsp</label>
            <input class="postcodeinput" type="text" name='stad'  placeholder="Stad" required/>
        </div>
    </div>
    <button type="submit" class="btn btn-success" style="float: right">Submit</button>
</form>
</div>
