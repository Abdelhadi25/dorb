<?php
/** @var PDO $conn */

session_start();

include '../../private/connection.php';

$aantal = ($_POST['aantal']);
$soort = ($_POST['soortpallet']);
$beschrijving = ($_POST['beschrijving']);
$postcode = ($_POST['postcode']);
$huisnummer = ($_POST['huisnummer']);
$straat = ($_POST['straat']);
$stad = ($_POST['stad']);
$status = '1';


$sql = 'SELECT maat FROM soortpallets WHERE soort_id = :soort_id';
$sth = $conn->prepare($sql);
$sth->execute(array(
    ':soort_id' => $soort
));
$result = $sth->fetch();

$maat = $result['maat'] * $aantal;

$sql = 'SELECT klant_id FROM klanten WHERE FK_login_id = :FK_login_id';
$sth = $conn->prepare($sql);
$sth->execute(array(
    ':FK_login_id' => $_SESSION['id'],
));

$klantnummer = $sth->fetch();

$sql = 'INSERT INTO orders (aantal, soort, maat,beschrijving, postcode, huisnummer, straat, stad, FK_klantnummer,status) VALUES (:aantal,:soort,:maat,:beschrijving, :postcode, :huisnummer, :straat, :stad, :FK_klantnummer,:status)';
$sth = $conn->prepare($sql);
$sth->execute(array(
    ':aantal' => $aantal,
    ':soort' => $soort,
    ':maat' => $maat,
    ':beschrijving' => $beschrijving,
    ':postcode' => $postcode,
    ':huisnummer' => $huisnummer,
    ':straat' => $straat,
    ':stad' => $stad,
    ':FK_klantnummer' => $klantnummer['klant_id'],
    ':status' => $status
));

header('location:../index.php?page=mijnorders');


