<?php

session_start();

include '../../Private/connection.php';


    $gebruikersnaam = ($_POST['gebruikersnaam']);
    $wachtwoord = ($_POST['wachtwoord']);
    $voornaam = ($_POST['voornaam']);
    $tussenvoegsel = ($_POST['tussenvoegsel']);
    $achternaam = ($_POST['achternaam']);
    $login_id = ($_POST['loginid']);
    $rol = ($_POST['rol']);
    $id = ($_POST['id']);

    $hashed_wachtwoord = password_hash($wachtwoord, PASSWORD_DEFAULT);


    if ($rol = 'helpdesk') {

        $sql = 'UPDATE helpdesk SET  `voornaam` = :voornaam, `tussenvoegsel` = :tussenvoegsel ,`achternaam` = :achternaam WHERE helpdesk_id = :helpdesk_id ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(array(
            ':voornaam' => $voornaam,
            ':tussenvoegsel' => $tussenvoegsel,
            ':achternaam' => $achternaam,
            ':helpdesk_id' => $id
        ));

        $sql = 'UPDATE login SET `gebruikersnaam`= :gebruikersnaam, `wachtwoord` = :wachtwoord  WHERE login_id = :login_id ';

        $stmt = $conn->prepare($sql);

        $stmt->execute(array(
            ':gebruikersnaam' => $gebruikersnaam,
            ':wachtwoord' => $hashed_wachtwoord,
            ':login_id' => $login_id

        ));
        header('location:../index.php?page=manager');
    }
    if ($rol = 'helpdesk') {

        $sql = 'UPDATE planners SET  `voornaam` = :voornaam, `tussenvoegsel` = :tussenvoegsel ,`achternaam` = :achternaam WHERE planner_id = :planner_id ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(array(
            ':voornaam' => $voornaam,
            ':tussenvoegsel' => $tussenvoegsel,
            ':achternaam' => $achternaam,
            ':planner_id' => $id
        ));

        $sql = 'UPDATE login SET `gebruikersnaam`= :gebruikersnaam, `wachtwoord` = :wachtwoord  WHERE login_id = :login_id ';

        $stmt = $conn->prepare($sql);

        $stmt->execute(array(
            ':gebruikersnaam' => $gebruikersnaam,
            ':wachtwoord' => $hashed_wachtwoord,
            ':login_id' => $login_id

        ));
        header('location:../index.php?page=manager');
    }



?>