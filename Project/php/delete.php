<?php

include '../../Private/connection.php';
if ($_POST["rol"] = 'helpdesk') {
    if (isset($_POST["id"])) {

        $stmt = $conn->prepare("DELETE  FROM helpdesk WHERE helpdesk_id = :id;");
        $stmt->bindParam(':id', $_POST["id"], PDO::PARAM_INT);
        $stmt->execute();

    }

    if (isset($_POST["login_id"])) {

        $stmt = $conn->prepare("DELETE  FROM login WHERE login_id = :login_id;");
        $stmt->bindParam(':login_id', $_POST["login_id"], PDO::PARAM_INT);
        $stmt->execute();


    }
    header('location:../index.php?page=manager');

}
if ($_POST["rol"] = 'planner') {
    if (isset($_POST["id"])) {

        $stmt = $conn->prepare("DELETE  FROM planners WHERE planner_id = :id;");
        $stmt->bindParam(':id', $_POST["id"], PDO::PARAM_INT);
        $stmt->execute();

    }

    if (isset($_POST["login_id"])) {

        $stmt = $conn->prepare("DELETE  FROM login WHERE login_id = :login_id;");
        $stmt->bindParam(':login_id', $_POST["login_id"], PDO::PARAM_INT);
        $stmt->execute();


    }
    header('location:../index.php?page=manager');

}
?>