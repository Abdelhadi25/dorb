<?php
include '../../Private/connection.php';

$gebruikersnaam = ($_POST['gebruikersnaam']);
$wachtwoord = ($_POST['wachtwoord']);
$voornaam = ($_POST['voornaam']);
$tussenvoegsel = ($_POST['tussenvoegsel']);
$achternaam = ($_POST['achternaam']);
$rol = "5";

$hashed_wachtwoord = password_hash($wachtwoord, PASSWORD_DEFAULT);


$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES,TRUE);
$sql = 'INSERT INTO login (gebruikersnaam, wachtwoord, FK_rol_id) VALUES (:gebruikersnaam,:wachtwoord,:rol)';
$sth = $conn->prepare($sql);
$sth->execute(array(
    ':gebruikersnaam' => $gebruikersnaam,
    ':wachtwoord' => $hashed_wachtwoord,
    ':rol' => $rol
));

$rsid = $conn -> lastInsertId();

$sql = 'INSERT INTO helpdesk ( voornaam, tussenvoegsel,achternaam,FK_login_id) VALUES ( :voornaam,:tussenvoegsel ,:achternaam, :FK_login_id)';
$sth = $conn->prepare($sql);
$sth->execute(array(
    ':voornaam' => $voornaam,
    ':tussenvoegsel' => $tussenvoegsel,
    ':achternaam' => $achternaam,
    ':FK_login_id' => (int)$rsid

));
header("Location: ../index.php?page=manager");

?>