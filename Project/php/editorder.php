<?php
session_start();

include '../../private/connection.php';

$id = $_GET['id'];

$aantal = ($_POST['aantal']);
$soort = ($_POST['soortpallet']);
$beschrijving = ($_POST['beschrijving']);
$postcode = ($_POST['postcode']);
$huisnummer = ($_POST['huisnummer']);
$straat = ($_POST['straat']);
$stad = ($_POST['stad']);

$sql = 'UPDATE orders SET  `aantal` = :aantal, `soort` = :soort ,`beschrijving` = :beschrijving, `postcode` = :postcode, `huisnummer` = :huisnummer, `straat` = :straat, `stad` = :stad WHERE order_id = :order_id ';
$stmt = $conn->prepare($sql);
$stmt->execute(array(
    ':aantal' => $aantal,
    ':soort' => $soort,
    ':beschrijving' => $beschrijving,
    ':postcode' => $postcode,
    ':huisnummer' => $huisnummer,
    ':straat' => $straat,
    ':stad' => $stad,
    ':order_id' => $id
));


header('location:../index.php?page=mijnorders');


?>