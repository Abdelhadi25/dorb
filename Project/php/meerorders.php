<?php

session_start();
include '../../private/connection.php';

$status = '2';


$sql = 'UPDATE orders SET status = :status WHERE order_id = :order_id';
$sth = $conn->prepare($sql);
$sth ->execute(array(
    ':status' => $status,
    ':order_id' => $_POST['orderid']
));

$sql = 'SELECT SUM(orders.maat) AS maat FROM koppeltbl INNER JOIN orders ON FK_order_id = order_id WHERE rit_id = :rit_id';
$sth = $conn->prepare($sql);
$sth->execute(array(
    ':rit_id' => $_POST['ritid']
));
$result = $sth->fetch();


$sql = 'SELECT inhoud FROM vrachtwagen INNER JOIN soortvrachtwagen ON FK_soort_id = soort_id WHERE vrachtwagen_id = :vrachtwagen_id';
$sth = $conn->prepare($sql);
$sth->execute(array(
    ':vrachtwagen_id' => $_POST['vrachtwagenid']
));
$resultvrachtwagen = $sth->fetch();

$sql = 'INSERT INTO koppeltbl (FK_order_id,rit_id ,FK_vrachwagen_id, FK_chauffeur_id, datum) VALUE (:FK_order_id, :rit_id ,:FK_vrachwagen_id,:FK_chauffeur_id,now())';
$sth = $conn ->prepare($sql);
$sth->execute(array(
    ':FK_order_id' => $_POST['orderid'],
    ':rit_id' => $_POST['ritid'],
    ':FK_vrachwagen_id' => $_POST['vrachtwagenid'],
    ':FK_chauffeur_id' => $_POST['chauffeurid']
));

if ((int)$resultvrachtwagen - (int)$result > 10) {

header('location:../index.php?page=orders');
}
else{
    header('location:../index.php?page=vrachtwagenstatus&vw='.$_POST['vrachtwagenid'].'&mt='.$result['maat']);
}

?>

