<?php

session_start();
include '../../private/connection.php';


$soort = $_POST['soort'];
$kenteken = $_POST['kenteken'];
$apk = $_POST['apk'];
$status = $_POST['status'];
$id = $_GET['id'];

$sql = 'UPDATE vrachtwagen SET `FK_soort_id` = :soort, `kenteken` = :kenteken, `apk` = :apk, `status` = :status WHERE vrachtwagen_id = :id';
$sth = $conn->prepare($sql);
$sth ->execute(array(
   ':soort' =>  $soort,
    ':kenteken' => $kenteken,
    ':apk' => $apk,
    ':status' => $status,
    ':id' => $id
));

header('location:../index.php?page=planner');



?>