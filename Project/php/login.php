<?php
session_start();

$_SESSION['rol'] = false;

include '../../private/connection.php';



$sql = 'SELECT login_id,wachtwoord ,rol FROM login INNER JOIN rollen ON FK_rol_id = rol_id WHERE gebruikersnaam = :gebruikersnaam ';
$sth = $conn->prepare($sql);

$sth->execute(array(
    ":gebruikersnaam" => $_POST["gebruikersnaam"]
));
$result = $sth->fetch();


if (password_verify($_POST["wachtwoord"] , $result['wachtwoord'])) {

$_SESSION['id'] = $result['login_id'];
$_SESSION['rol'] = $result['rol'];

//echo $result['rol'];

switch ($result['rol']) {
    case 'klant':
        header('location:../index.php?page=klant');
        break;
    case 'manager':
        header('location:../index.php?page=manager');
        break;
    case 'helpdesk':
        header('location:../index.php?page=helpdesk');
        break;
    case 'planner':
        header('location:../index.php?page=planner');
        break;
}

} else {
   $_SESSION['melding'] = '<label>Gebriukersnaam of wachtwoord zijn onjuist</label>';
    header('location:../index.php?page=home');
}

?>
