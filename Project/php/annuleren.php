<?php

session_start();

include '../../private/connection.php';

$id = $_GET['id'];

$sql = 'DELETE FROM orders WHERE order_id = :order_id';
$sth = $conn ->prepare($sql);
$sth ->execute(array(
    ':order_id' => $id
));

header('location:../index.php?page=mijnorders');

?>