<?php

include '../../Private/connection.php';


if (!isset($error)) {
    $gebruikersnaam = ($_POST['gebruikersnaam']);
    $wachtwoord = ($_POST['wachtwoord']);
    $voornaam = ($_POST['voornaam']);
    $tussenvoegsel = ($_POST['tussenvoegsel']);
    $achternaam = ($_POST['achternaam']);
    $geboortedatum = ($_POST['geboortedatum']);
    $woonplaats = ($_POST['woonplaats']);
    $geslacht = ($_POST['geslacht']);
    $rol = "4";


    $hashed_wachtwoord = password_hash($wachtwoord, PASSWORD_DEFAULT);


    $sth = $conn->prepare("SELECT gebruikersnaam FROM login WHERE gebruikersnaam = :gebruikersnaam");
    $sth->bindParam(':gebruikersnaam', $gebruikersnaam);
    $sth->execute();
    $result = $sth->fetchAll();

    if (!empty($result)) {
        echo "Gebruikersnaam bestaat al";
    } else {
        $sql = 'INSERT INTO login (gebruikersnaam, wachtwoord, FK_rol_id) VALUES (:gebruikersnaam,:wachtwoord,:FK_rol_id)';
        $sth = $conn->prepare($sql);
        $sth->execute(array(
            ':gebruikersnaam' => $gebruikersnaam,
            ':wachtwoord' => $hashed_wachtwoord,
            ':FK_rol_id' => $rol
        ));

        $rsid = $conn -> lastInsertId();

        $sql = 'INSERT INTO klanten ( voornaam, tussenvoegsel ,achternaam, geboortedatum, woonplaats, geslacht ,FK_login_id ) VALUES (:voornaam, :tussenvoegsel,:achternaam, :geboortedatum, :woonplaats, :geslacht,:FK_login_id)';
        $sth = $conn->prepare($sql);
        $sth->execute(array(
            ':voornaam' => $voornaam,
            ':tussenvoegsel' => $tussenvoegsel,
            ':achternaam' => $achternaam,
            ':geboortedatum' => $geboortedatum,
            ':woonplaats' => $woonplaats,
            ':geslacht' => $geslacht,
            ':FK_login_id' => (int)$rsid,

        ));
        header("Location: ../index.php?page=registeereenklant");
    }
}
?>
